import java.io.BufferedReader;
import java.io.InputStreamReader;
/**
 * 
 * @author Hector Florez
 *
 */
public class SoldiersAndBananas {
	
	public static void main(String[] args) {
		try {									
			BufferedReader br = new BufferedReader(new InputStreamReader(System.in));			
			String datos[] = br.readLine().split(" ");
			int k = Integer.parseInt(datos[0]);
			int n = Integer.parseInt(datos[1]);
			int w = Integer.parseInt(datos[2]);
			for (int i=1; i<=w; i++) {
				n -= i*k;
			}
			if(n<0) {
				System.out.println(-n);
			}else {
				System.out.println("0");
			}
			br.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}