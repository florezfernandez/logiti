import java.io.BufferedReader;
import java.io.InputStreamReader;
/**
 * 
 * @author Hector Florez
 *
 */
public class Tram {
	public static void main(String[] args) {
		try {
			BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
			int n = Integer.parseInt(br.readLine());
			int mayor = 0;
			int total = 0;
			while(n-- > 0) {				
				String parada[] = br.readLine().split(" "); 
				total -= Integer.parseInt(parada[0]);
				total += Integer.parseInt(parada[1]);
				if (total > mayor) {
					mayor = total;
				}
			}			
			System.out.println(mayor);
			br.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}	
}
