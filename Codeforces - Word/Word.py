# @author Hector Florez
palabra = input()
min = 0
may = 0
for car in palabra:
    if car.islower():
        min += 1
    else:
        may += 1
print(palabra.lower() if (min>=may) else palabra.upper())