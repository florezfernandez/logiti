import java.io.BufferedReader;
import java.io.InputStreamReader;
/**
 * 
 * @author Hector Florez
 *
 */
public class Word {

	public static void main(String[] args) {
		try {
			BufferedReader br = new BufferedReader(new InputStreamReader(System.in));			
			String palabra = br.readLine();
			char []letras = palabra.toCharArray();
			int may = 0;
			int min = 0;
			for(int i=0; i<letras.length; i++) {
				if(letras[i]>= 97) {
					min++;
				}else {
					may++;
				}
			}
			System.out.println((min>=may)?palabra.toLowerCase():palabra.toUpperCase());
			br.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
