# @author Daniel Sanchez
def solve(a, b, n, m):
    A = n // (m + 1)
    B = n - (A * (m+1))
    R = A * min(a * m, b * (m + 1)) + B * min(a, b)
    return R

if __name__ == '__main__':
    t = int(input())
    for _ in range(t):
        a, b = [int(e) for e in input().split()]
        n, m = [int(e) for e in input().split()]
        print(solve(a, b, n, m))
