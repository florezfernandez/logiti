/**
 * @author Hector Florez
 */

import java.util.Scanner;

public class VikaAndHerFriends {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int t = sc.nextInt();
		while(t-- > 0) {
			int n = sc.nextInt();
			int m = sc.nextInt();
			int k = sc.nextInt();
			int x = sc.nextInt();
			int y = sc.nextInt();
			if(captura(x,y,k,sc)) {
				System.out.println("NO");
			}else {
				System.out.println("YES");
			}
		}
		sc.close();
	}

	private static boolean captura(int x, int y, int k, Scanner sc) {
		boolean captura = false;
		for(int i=0; i<k; i++) {
			int xa = sc.nextInt();
			int ya = sc.nextInt();
			int dist = Math.abs(x-xa) + Math.abs(y-ya);
			if (dist%2 == 0) {
				captura = true;
			}
		}
		return captura;
	}
}
