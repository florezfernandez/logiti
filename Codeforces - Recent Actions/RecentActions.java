import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.StringTokenizer;
/**
 * 
 * @author Hector Florez
 *
 */
public class RecentActions {
	public static void main(String[] args) throws NumberFormatException, IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int t = Integer.parseInt(br.readLine());
		while(t-- > 0) {
			String datos[] = br.readLine().split(" ");
			int n = Integer.parseInt(datos[0]);
			int m = Integer.parseInt(datos[1]);
			int listaM[] = new int[m];
			StringTokenizer st = new StringTokenizer(br.readLine());
			for(int i=0; i<m; i++) {
				listaM[i] = Integer.parseInt(st.nextToken());
			}
			int res[] = evaluar(n, m, listaM);
			for(int i=0; i<n; i++) {
				System.out.print((res[i]==0)?"-1 ":res[i]+" ");
			}
			System.out.println();
		}
	}

	private static int[] evaluar(int n, int m, int[] listaM) {
		HashMap<Integer, Integer> dic = new HashMap<Integer, Integer>();
		int resultado[] = new int[n];
		for(int i=0; i<m; i++) {
			if(n == 0) {
				return resultado;
			}
			if(!dic.containsKey(listaM[i])) {				
				dic.put(listaM[i], 1);
				resultado[--n] = i+1;
			}
		}
		return resultado;
	}
}
