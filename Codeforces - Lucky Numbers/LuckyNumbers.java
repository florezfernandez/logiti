/**
 * @author Hector Florez
 */
import java.util.Scanner;
public class LuckyNumbers {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);		
		System.out.println((long)Math.pow(2, sc.nextInt()+1)-2);
		sc.close();
	}
}
