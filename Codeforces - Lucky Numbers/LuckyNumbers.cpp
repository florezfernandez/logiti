// @author Hector Florez
#include <iostream>
#include <cmath>
using namespace std;
int main(){
    int n;
    cin >> n;
    cout << (long long)(pow(2,n+1))-2 << endl;
}
