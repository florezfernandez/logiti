import java.util.Scanner;
/**
 * 
 * @author Hector Florez
 *
 */
public class MrPerfectlyFine {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int t = sc.nextInt();
		while(t-- > 0) {
			int n = sc.nextInt();
			int t1 = 200001;
			int t2 = 200001;
			int tIguales = 200001; 
			boolean h1 = false;
			boolean h2 = false;
			while (n-- > 0) {
				int min = sc.nextInt();
				char []h = sc.next().toCharArray();
				if(h[0] == '1' && h[1] == '1') {
					if(min<tIguales) {
						tIguales = min;
						h1 = true;
						h2 = true;
					}
				}				
				if (h[0] == '1') {
					if (min<t1) {
						t1 = min;
						h1 = true;
					}
				}
				if (h[1] == '1') {
					if (min<t2) {
						t2 = min;
						h2 = true;
					}
				}
			}
			if(!h1 || !h2) {
				System.out.println("-1");
			}else if(tIguales < t1+t2 && tIguales!=200001){
				System.out.println(tIguales);
			}else {
				System.out.println(t1 + t2);
			}
		}
		sc.close();
	}
}
