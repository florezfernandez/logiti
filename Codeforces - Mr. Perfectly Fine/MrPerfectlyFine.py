# https://codeforces.com/problemset/problem/1829/C

# Time complexity: O(N) Linear time complexity
# Space complexity: O(1) Constant space complexity
# @author Daniel Sanchez

t = int(input())
for _ in range(t):
    n = int(input())
    _min = {
        '11': 200001,
        '10': 200001,
        '01': 200001
    }
    
    for _ in range(n):
        m, b = input().split()
        if b != '00':
            _min[b] = min(_min[b],int(m))
    
    if _min['11'] == 200001 and (_min['10'] == 200001 or _min['01'] == 200001):
        print('-1')
    else:
        r = _min['10'] + _min['01']
        if _min['11'] == 200001:
            print(r)
        else:
            print(_min['11'] if _min['11'] < t else t)
