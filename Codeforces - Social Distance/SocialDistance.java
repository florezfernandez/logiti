import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.StringTokenizer;
/**
 * 
 * @author Hector Florez
 *
 */

public class SocialDistance {
	public static void main(String[] args) throws Exception {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int t = Integer.parseInt(br.readLine());
		while (t-- > 0) {
			String datos[] = br.readLine().split(" ");
			int n = Integer.parseInt(datos[0]);
			int m = Integer.parseInt(datos[1]);
			StringTokenizer st = new StringTokenizer(br.readLine());
			int espacios[] = new int[n];
			for(int i=0; i<n; i++) {
				espacios[i] = Integer.parseInt(st.nextToken());
			}
			Arrays.sort(espacios);			
			System.out.println(evaluar(m, espacios)?"YES":"NO");
		}
	}

	private static boolean evaluar(int m, int[] espacios) {
		int sillas = 0;				
		for(int i=espacios.length-1; i>=0; i--) {
			sillas += 1 + espacios[i];
			if(sillas > m) {
				return false;
			}
		}
		if(sillas + espacios[espacios.length-1] - espacios[0] > m) {
			return false;
		}else {
			return true;	
		}		
	}
}
