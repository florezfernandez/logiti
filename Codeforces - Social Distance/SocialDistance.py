# @author Daniel Sanchez
def solve(n, m, a):
    c = sum(a) - min(a) + max(a) + n
    return 'YES' if c <= m else 'NO'

t = int(input())
for _ in range(t):
    n, m = [int(e) for e in input().split()]
    a = [int(e) for e in input().split()]
    print(solve(n, m, a))
