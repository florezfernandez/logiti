import java.util.Scanner;
/**
 * 
 * @author Hector Florez
 *
 */
public class Dictionary {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int t = sc.nextInt();
		while(t-- > 0) {
			String n = sc.next();
			int l1 = n.charAt(0) - 96;
			int l2 = n.charAt(1) - 96;
			int pos = (l1-1)*25 + l2;
			if(l2 > l1) {
				pos--;
			}
			System.out.println(pos);
		}
		sc.close();
	}
}
