/**
 * @author Hector Florez
 */
import java.util.Scanner;

public class ShuffleParty {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int t = sc.nextInt();
		while(t-- > 0) {
			int n = sc.nextInt();
			System.out.println((int)(Math.pow(2, (int)(Math.log(n)/Math.log(2)))));			
		}
		sc.close();
	}
}
