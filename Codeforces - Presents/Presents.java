import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Presents {

	public static void main(String[] args) {
		try {						
			BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
			int n = Integer.parseInt(br.readLine());
			String p[] = br.readLine().split(" ");
			for(int i=1; i<=n; i++) {
				System.out.print(buscar(i,p) + " ");
			}
			br.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private static int buscar(int n, String[] p) {
		for(int i=0; i<p.length; i++) {
			if(Integer.parseInt(p[i]) == n) {
				return i+1;
			}
		}
		return -1;
	}
}
