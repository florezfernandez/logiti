# @author David Tabla
friends_quantity = int(input())

receivers = {
    int(r): i+1
    for i, r in enumerate(input().split(" "))
}

senders = []
for i in range(len(receivers)):
    senders.append(str(receivers[i+1]))
    
print(" ".join(senders))