// @author Hector Florez
#include <iostream>
#include <algorithm>
using namespace std;
int main(){
    int n, q;
    cin >> n >> q;
    long long precios[n];
    for(int i=0; i<n; i++){
        cin >> precios[i];
    }
    sort(precios, precios + sizeof(precios)/sizeof(precios[0]), greater<long>());
    long long preciosAcumulados[n];
    preciosAcumulados[0] = precios[0];
    for(int i=1; i<n; i++){
        preciosAcumulados[i] = preciosAcumulados[i-1] + precios[i];
    }
    while(q-- > 0){
        int x, y;
        cin >> x >> y;
        if(x != y){
            cout << preciosAcumulados[x-1] - preciosAcumulados[x-y-1] << endl;
        } else {
            cout << preciosAcumulados[x-1] << endl;
        }
    }
}
