#@author Hector Florez
s = input().split()
n = int(s[0])
q = int(s[1])
precios = [int(i) for i in input().split()]
precios.sort(reverse=True)
preciosAcumulados = [0]*n
preciosAcumulados[0] = precios[0]
for i in range(1,n):
    preciosAcumulados[i] = preciosAcumulados[i-1] + precios[i]
while q > 0:
    s = input().split()
    x = int(s[0])
    y = int(s[1])
    if x != y:
        print (preciosAcumulados[x-1] - preciosAcumulados[x-y-1])
    else:
        print (preciosAcumulados[x-1])
    q -= 1