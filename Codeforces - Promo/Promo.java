import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.Collections;
import java.util.StringTokenizer;
/**
 * 
 * @author Hector Florez
 *
 */
public class Promo {

	public static void main(String[] args) {
		try {
			BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
			StringTokenizer st;
			st = new StringTokenizer(br.readLine());
			int n = Integer.parseInt(st.nextToken());
			int q = Integer.parseInt(st.nextToken());
			Long precios[] = new Long[n];
			st = new StringTokenizer(br.readLine());
			for(int i=0; i<n; i++) {
				precios[i] = Long.parseLong(st.nextToken());
			}
			Arrays.sort(precios, Collections.reverseOrder());
			Long preciosTotales[] = new Long[n];
			preciosTotales[0] = precios[0];
			for(int i=1; i<n; i++) {
				preciosTotales[i] = preciosTotales[i-1] + precios[i];
			}
			while(q-- > 0) {				
				st = new StringTokenizer(br.readLine());
				int x = Integer.parseInt(st.nextToken());
				int y = Integer.parseInt(st.nextToken());				
				if(x != y) {
					long total = preciosTotales[x-1] - preciosTotales[x-y-1];
					System.out.println(total);					
				}else {
					System.out.println(preciosTotales[x-1]);
				}
			}
			br.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
