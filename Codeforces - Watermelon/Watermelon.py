# @author Hector Florez
peso = int(input())
if peso != 2 and peso%2 == 0:
    print("YES")
else:
    print("NO")