import java.io.BufferedReader;
import java.io.InputStreamReader;
/**
 * 
 * @author Hector Florez
 *
 */
public class Watermelon {
	public static void main(String[] args) {
		try {
			BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
			int peso = Integer.parseInt(br.readLine());
			if(peso != 2 && peso%2 == 0) {
				System.out.println("YES");
			}else {
				System.out.println("NO");
			}
			br.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}	
}
