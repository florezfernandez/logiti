import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
/**
 * 
 * @author Hector Florez
 *
 */
public class Stripes {

	public static void main(String[] args) {
		try {
			BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
			int casos = Integer.parseInt(br.readLine());
			while(casos-- > 0) {
				br.readLine();
				System.out.println(evaluarCaso(br));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private static char evaluarCaso(BufferedReader br) throws IOException {
		boolean esRojo = false;
		for(int i=0; i<8; i++) {
			String linea = br.readLine();
			if(!esRojo) {
				if(evaluarFila(linea)) {
					esRojo = true;
				}				
			}
		}
		return (esRojo)?'R':'B';
	}

	private static boolean evaluarFila(String linea) {
		for(int i=0; i<8; i++) {
			if(linea.charAt(i) != 'R') {
				return false;
			}
		}
		return true;
	}
}
