# @author Daniel Sanchez
def solve(a):
    diff = set(a)
    num = len(diff)
    r = [str(num)]*num
    for _ in range(num, len(a)):
        num += 1
        r.append(str(num))
    return ' '.join(r)

t = int(input())
for _ in range(t):
    n = input()
    a = [int(e) for e in input().split()]
    print(solve(a))
